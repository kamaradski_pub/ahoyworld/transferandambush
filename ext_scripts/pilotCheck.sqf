/*
Modified by [LB] chucky to temporarily disable copilot on transport choppers whilst bug 11987 is outstanding
http://feedback.arma3.com/view.php?id=11987

Adapted SaMatra's code published on:
http://forums.bistudio.com/showthread.php?157481-crewmen

*** Pilots & Crew ***
"B_Helipilot_F"
"B_Pilot_F"
"B_helicrew_F"
"B_Story_Pilot_F"
"O_helipilot_F"
"O_Pilot_F"
"O_helicrew_F"
"I_Soldier_04_F"
"I_helipilot_F"
"I_pilot_F"
"C_man_pilot_F"

*** Engineers ***
"B_engineer_F"
"B_Story_Engineer_F"
"B_CTRG_soldier_engineer_exp_F"
"O_G_engineer_F"
"O_engineer_F"
"O_engineer_U_F"
"I_G_engineer_F"
"I_engineer_F"


*/
#define REJ_MSG "You must be a pilot to fly this aircraft"

true spawn {

    _pilots = ["B_Helipilot_F","B_Pilot_F","B_helicrew_F","B_Story_Pilot_F","O_helipilot_F","O_Pilot_F","O_helicrew_F","I_Soldier_04_F","I_helipilot_F","I_pilot_F","C_man_pilot_F","B_engineer_F","B_Story_Engineer_F","B_CTRG_soldier_engineer_exp_F","O_G_engineer_F","O_engineer_F","O_engineer_U_F","I_G_engineer_F","I_engineer_F"];
//    _aircraft_nocopilot = ["B_Heli_Transport_01_camo_F", "B_Heli_Transport_01_F", "I_Heli_Transport_02_F", "O_Heli_Light_02_F", "O_Heli_Light_02_unarmed_F", "B_Heli_Light_01_armed_F"];

    waitUntil {player == player};

    _iampilot = ({typeOf player == _x} count _pilots) > 0;

    while { true } do {
        _oldvehicle = vehicle player;
        waitUntil {vehicle player != _oldvehicle};

        if(vehicle player != player) then {
            _veh = vehicle player;

            //------------------------------ pilot can be pilot seat only
			
            if((_veh isKindOf "Helicopter" || _veh isKindOf "Plane") && !(_veh isKindOf "ParachuteBase")) then {
//				if(({typeOf _veh == _x} count _aircraft_nocopilot) > 0) then {
//					_forbidden = [_veh turretUnit [0]];
//					if(player in _forbidden) then {
//						systemChat "Co-pilot is disabled on this vehicle";
//						player action ["getOut", _veh];
//					};
//				};
				if(!_iampilot) then {
					_forbidden = [driver _veh];
					if(player in _forbidden) then {
						titleText [REJ_MSG, "PLAIN", 3];
						player action ["getOut", _veh];
					};
				};
            };
		};
    };
}; 

