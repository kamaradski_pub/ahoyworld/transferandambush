//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			init.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// INIT script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Generic INIT settings:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

enableSaving [false, false];
enableSentences false;




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Below will run on both Server AND Client:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_null = [] execVM "int_scripts\kmissionStart.sqf";




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Grimes Simple Revive v07:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

G_isDedicated = false;
G_isServer = false;
G_isClient = false;
G_isJIP = false;
if (isDedicated) then {
    G_isDedicated = true;
    G_isServer = true;
}
else
{
    if (isServer) then {G_isServer = true};
    G_isClient = true;
    if (isNull player) then {G_isJIP = true};
    waitUntil {!isNull player};
};

_null = [] execVM "G_Revive_init.sqf";




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Task Management & Briefing:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[[
  ],[
  ["Mission Intel","Greetings BLUFOR,<br /><br />We all heard about the success of operation Invade and Annex, where we managed to drive most OPFOR off the island. After this intensive battle our troops are left with little supplies.<br /><br />Now today it is our mission to reinforce our troops in the field.<br /><br />There is some cargo next to the hanger that needs loading in the truck(s) and taken to different locations. HQ will inform you where to go as soon as you loaded the cargo.<br /><br />Good luck, and be careful out there to avoid the left over OPFOR resistance",WEST],
 ["Mission Intel","Greetings OPFOR,<br /><br />Now in all seriousness we took a severe beating during the BLUFOR mission Invade and Annex, however we did great and managed to retreat our best troops into hiding.<br /><br />Today we got intel that BLUFOR is running out of steam and supplies. If we can intercept the supplies that are being taken to the BLUFOR troops and use them to reinforce our own troops we will make a very good chance to plane a counter offence and regain Stratis.<br /><br />Our intel suggest that these resupply convoys are leaving the BLUFOR base shortly. Go prepare an ambush and let get those supplies for ourself!<br /><br />Good luck and see you back here with the goodies!",EAST],
["The Team","kamaradski - Lead designer and coder<br /><br />Community home: http://www.Ahoyworld.co.uk<br /> Mission home: https://bitbucket.org/kamaradski/t-a/<br /><br />"],
 ["Credits","
		David - Loading screen logo<br />
		Jester - Several bits &amp; bobs<br />
		StuffedSheep - Several bits &amp; bobs<br />
		Razgriz33 - Several bits &amp; bobs<br />
		Igi_PL - IgiLoad script<br />
		Grimes [3rd ID] - Revive Script<br />
		Tophe [OOPS] - Vehicle Respawn Script<br />
		Shuko - SHK_Taskmaster<br />
		code34 - Real Weather<br />
		Champ-1 - CH View Distance<br />
		1PARA{God-Father} - Disable 3rd person<br />
		<br />
		Special Thanks:<br/>Everyone @ AhoyWorld.co.uk - Mission testing and hosting<br /><br />"
	]
]] call compile preprocessfilelinenumbers "ext_scripts\shk_taskmaster\shk_taskmaster.sqf";




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// IGILoad Script:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_igiload = execVM "ext_scripts\IgiLoad\IgiLoadInit.sqf";

