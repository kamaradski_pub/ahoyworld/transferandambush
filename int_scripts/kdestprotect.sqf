// Select sides
	_side1 = west;
	_side2 = east;

_kwhere = _this select 0;

// hint parseText format["%1", getMarkerPos "KVIKOS"];
	
if (_kwhere == "desvikos") then { 
	kdestPos = getMarkerPos "KVIKOS";
	kstatic1 = [12170.1,8439.14,0]; // AA
	kstatic2 = [12107.2,8407.08,0]; // Tank
	kstatic3 = [12158.4,8416.18,0]; // troops
};

if (_kwhere == "desathanos") then { 
	kdestPos = getMarkerPos "kAthanos";
	kstatic1 = [2752.71,9894.43,0]; // AA
	kstatic2 = [2799.06,9894.25,0]; // Tank
	kstatic3 = [2653.65,9866.15,0]; // troops
};

if (_kwhere == "descastle") then { 
	kdestPos = getMarkerPos "kCASTLE";
	kstatic1 = [4755.74,21918.6,0]; // AA
	kstatic2 = [4946.57,21882,0]; // Tank
	kstatic3 = [4829.61,21929.4,0]; // troops
};

if (_kwhere == "desselakano") then { 
	kdestPos = getMarkerPos "kSELAKANO";
	kstatic1 = [20727.5,6839.07,0]; // AA
	kstatic2 = [20803.4,6797.66,0]; // Tank
	kstatic3 = [20795,6762.25,0]; // troops
};

if (_kwhere == "desgori") then { 
	kdestPos = getMarkerPos "kGORI";
	kstatic1 = [5374.58,17922,0]; // AA
	kstatic2 = [5447.69,17942.4,0]; // Tank
	kstatic3 = [5415.91,17900,0]; // troops
};


// spawn static troops
	_sgrp1 = createGroup _side1; 
	_sgrp1 = [ kstatic1, 316, "B_APC_Tracked_01_AA_F", _side1 ] call BIS_fnc_spawnVehicle;
	_sgrp1 = _sgrp1 select 2;
	
	_sgr1wp0 = _sgrp1 addWaypoint [ kstatic1, 0 ]; 
	_sgr1wp0 setWaypointType "GUARD"; 
	_sgr1wp0 setWaypointBehaviour "COMBAT"; 
	_sgr1wp0 setwaypointcombatmode "RED"; 
	_sgr1wp0 setWaypointSpeed "LIMITED"; 
	_sgr1wp0 setWaypointCompletionRadius 10;  
	sleep 1;

	
	_sgrp2 = createGroup _side1; 
	_sgrp2 = [ kstatic2, 316, "B_MBT_01_cannon_F", _side1 ] call BIS_fnc_spawnVehicle;
	_sgrp2 = _sgrp2 select 2;
	
	_sgr2wp0 = _sgrp2 addWaypoint [ kstatic2, 0 ]; 
	_sgr2wp0 setWaypointType "GUARD"; 
	_sgr2wp0 setWaypointBehaviour "COMBAT"; 
	_sgr2wp0 setwaypointcombatmode "RED"; 
	_sgr2wp0 setWaypointSpeed "LIMITED"; 
	_sgr2wp0 setWaypointCompletionRadius 10;  
	sleep 1;

	_sgrp3 = createGroup _side1; 
	_sgrp3 = [ kstatic3, _side1, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >>  "BUS_InfTeam_AA")] call BIS_fnc_spawnGroup; 
	
	_sgr3wp0 = _sgrp3 addWaypoint [ kstatic3, 0 ]; 
	_sgr3wp0 setWaypointType "GUARD"; 
	_sgr3wp0 setWaypointBehaviour "SAFE"; 
	_sgr3wp0 setwaypointcombatmode "RED"; 
	_sgr3wp0 setWaypointSpeed "LIMITED"; 
	_sgr3wp0 setWaypointCompletionRadius 10;  
	sleep 1;

diag_log format ["%1: Transport & Ambush: Server spawned static destination protection AI:", time];
	
// spawn dynamic patrols
// Spawn group-1 (west - AA)
	kRandSpawnPos = [kdestPos, 1, 150, 1, 0, 20, 0] call BIS_fnc_findSafePos;
	_dgrp1 = createGroup _side1; 
	_dgrp1 = [ kRandSpawnPos, _side1, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >>  "BUS_InfTeam_AA")] call BIS_fnc_spawnGroup; 
	
	_dgr1wp0 = _dgrp1 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr1wp0 setWaypointType "SAD"; 
	_dgr1wp0 setWaypointFormation "WEDGE"; 
	_dgr1wp0 setWaypointBehaviour "AWARE"; 
	_dgr1wp0 setwaypointcombatmode "RED"; 
	_dgr1wp0 setWaypointSpeed "NORMAL"; 
	_dgr1wp0 setWaypointCompletionRadius 5;  
	_dgr1wp1 = _dgrp1 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr1wp1 setWaypointType "CYCLE"; 
	sleep 3; //reduce lag upon spawn

	
// Spawn group-2 (west - squad)
	kRandSpawnPos = [kdestPos, 1, 150, 1, 0, 20, 0] call BIS_fnc_findSafePos;
	_dgrp2 = createGroup _side1; 
	_dgrp2 = [ kRandSpawnPos, _side1, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >>  "BUS_InfSquad")] call BIS_fnc_spawnGroup; 
	
	_dgr2wp0 = _dgrp2 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr2wp0 setWaypointType "SAD"; 
	_dgr2wp0 setWaypointFormation "WEDGE"; 
	_dgr2wp0 setWaypointBehaviour "AWARE"; 
	_dgr2wp0 setwaypointcombatmode "RED"; 
	_dgr2wp0 setWaypointSpeed "NORMAL"; 
	_dgr2wp0 setWaypointCompletionRadius 5;  
	_dgr2wp1 = _dgrp2 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr2wp1 setWaypointType "CYCLE"; 
	sleep 3; //reduce lag upon spawn

	
// Spawn group-3 (west - Vehicle)
	kRandSpawnPos = [kdestPos, 1, 150, 1, 0, 20, 0] call BIS_fnc_findSafePos;
	_dgrp3 = createGroup _side1; 
	_dgrp3 = [ kRandSpawnPos, 316, "B_APC_Tracked_01_rcws_F", _side1 ] call BIS_fnc_spawnVehicle;
	_dgrp3 = _dgrp3 select 2;
	
	_dgr3wp0 = _dgrp3 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr3wp0 setWaypointType "SAD"; 
	_dgr3wp0 setWaypointFormation "WEDGE"; 
	_dgr3wp0 setWaypointBehaviour "AWARE"; 
	_dgr3wp0 setwaypointcombatmode "RED"; 
	_dgr3wp0 setWaypointSpeed "LIMITED"; 
	_dgr3wp0 setWaypointCompletionRadius 5;  
	_dgr3wp1 = _dgrp3 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr3wp1 setWaypointType "CYCLE"; 
	sleep 3; //reduce lag upon spawn


// Spawn group-4 (west - tank)
	kRandSpawnPos = [kdestPos, 1, 150, 1, 0, 20, 0] call BIS_fnc_findSafePos;
	_dgrp4 = createGroup _side1; 
	_dgrp4 = [ kRandSpawnPos, 316, "B_MBT_01_cannon_F", _side1 ] call BIS_fnc_spawnVehicle; 
	_dgrp4 = _dgrp4 select 2;
	
	_dgr4wp0 = _dgrp4 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr4wp0 setWaypointType "SAD"; 
	_dgr4wp0 setWaypointFormation "WEDGE"; 
	_dgr4wp0 setWaypointBehaviour "AWARE"; 
	_dgr4wp0 setwaypointcombatmode "RED"; 
	_dgr4wp0 setWaypointSpeed "LIMITED"; 
	_dgr4wp0 setWaypointCompletionRadius 5;  
	_dgr4wp1 = _dgrp4 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr4wp1 setWaypointType "CYCLE"; 
	"kmgrp4" setmarkerpos kRandSpawnPos;
	sleep 3; //reduce lag upon spawn


// Spawn group-5 (west - infan)
	kRandSpawnPos = [kdestPos, 1, 150, 1, 0, 20, 0] call BIS_fnc_findSafePos;
	_dgrp5 = createGroup _side1; 
	_dgrp5 = [ kRandSpawnPos, _side1, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >>  "BUS_InfSquad")] call BIS_fnc_spawnGroup; 
	
	_dgr5wp0 = _dgrp5 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr5wp0 setWaypointType "SAD"; 
	_dgr5wp0 setWaypointFormation "WEDGE"; 
	_dgr5wp0 setWaypointBehaviour "AWARE"; 
	_dgr5wp0 setwaypointcombatmode "RED"; 
	_dgr5wp0 setWaypointSpeed "NORMAL"; 
	_dgr5wp0 setWaypointCompletionRadius 5;  
	_dgr5wp1 = _dgrp5 addWaypoint [ kRandSpawnPos, 100 ]; 
	_dgr5wp1 setWaypointType "CYCLE"; 
	"kmgrp5" setmarkerpos kRandSpawnPos;

diag_log format ["%1: Transport & Ambush: Server spawned Dynamic destination protection AI:", time];
