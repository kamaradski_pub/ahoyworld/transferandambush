//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			kmissionStart.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// Mission start script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

diag_log format ["%1: Transport & Ambush: mission loaded", time];

kINTfblu 	= 0;
kINTfred 	= 0;
kINTsomewin = 0;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// SERVER countdown:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (isServer) then {
	missionNamespace setVariable ["kINTmissionstart",239];   			//set to:239
	publicVariable "kINTmissionstart";
	_kINTcountme =0;
	while {kINTmissionstart!=0} do {
		sleep 1;
		_kINTcountme = _kINTcountme + 1;
		if (_kINTcountme==5) then {
				publicVariable "kINTmissionstart";
				_kINTcountme=0;
			};
		kINTmissionstart = kINTmissionstart -1;
	};
	diag_log format ["%1: Transport & Ambush: Server finished countdown", time];
	publicVariable "kINTmissionstart";
	[] execVM "int_scripts\kmissionRun.sqf";								// Server starts and executes the mission
};



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// CLIENT countdown:
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (!isServer) then {
	kINTmissionstart = 400;
	"kINTmissionstart" addPublicVariableEventHandler {
		hint format ["mission start in: %1 seconds",kINTmissionstart];
		if (kINTmissionstart <= 1) then {
			diag_log format ["%1: Transport & Ambush: Client received countdown finished signal", time];
			[] execVM "int_scripts\kmissionEnd.sqf";						// Client jump to mission-end script and waits here till mission is over
		};
	};
};