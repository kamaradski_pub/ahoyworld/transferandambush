//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			ammo_box_special_red.sqf
// Author:		Kamaradski 2014
// Contributers:	Jester
//
// Crate loading script with restricted ammo for "Transport & Ambush"
// This script will respawn the ammobox content every 20 min
// _K = [this] execVM "int_scripts\ammo_box_special_red.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (!isServer) exitWith {};

_myBox = _this select 0;
_myBox allowDamage false; 

while {true} do {

	diag_log format ["%1: Transport & Ambush: (re)Spawned restricted ammo RED-UAV&Mortar:", time];
	clearWeaponCargoGlobal _myBox;
	clearMagazineCargoGlobal _myBox;
	clearBackpackCargoGlobal _myBox;
	clearItemCargoGlobal  _myBox;
	sleep 2;
	_myBox addBackpackCargoGlobal ["O_UAV_01_backpack_F",1];
	_myBox addBackpackCargoGlobal ["O_Mortar_01_support_F",2];

	sleep 1200; // Sleep 20 min before emptying the box, and respawn it's content
};
