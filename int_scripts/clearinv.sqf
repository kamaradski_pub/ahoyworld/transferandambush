//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			clearinv.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// Vehicle inventory clear script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


_veh = _this select 0;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
diag_log format ["%1: Transport & Ambush: Vehicle inventory emptied for: %2:", time, _veh];