//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			VA_west.sqf
// Author:		Kamaradski 2014
// Contributers:	Razgriz33
//
// 
// Drop-in Virtual Arsenal ammo-box for "Transport & Ambush"
// _K = [this] execVM "int_scripts\VA_west.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Setup VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_myBox = _this select 0;

_myBox allowDamage false; 
_myBox enableSimulation false;

clearWeaponCargo _myBox;
clearMagazineCargo _myBox;
clearBackpackCargo _myBox;
clearItemCargo _myBox;

["AmmoboxInit",[_myBox,true]] call BIS_fnc_arsenal;

	
	
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Empty VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[true],true] call BIS_fnc_removeVirtualBackpackCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualItemCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualWeaponCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualMagazineCargo;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add side independent gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"V_Chestrig_oli",
	"H_Shemag_olive",
	"ItemMap",
	"ItemCompass",
	"ItemWatch",
	"ItemRadio",
	"FirstAidKit",
	"V_BandollierB_blk",
	"H_Cap_oli",
	"V_Chestrig_blk",
	"H_Watchcap_blk",
	"V_TacVest_blk",
	"H_Booniehat_khk",
	"G_Goggles_VR",
	"H_Bandanna_khk",
	"H_Watchcap_camo",
	"V_BandollierB_khk",
	"ItemGPS"
],true] call BIS_fnc_addVirtualItemCargo;	

[_myBox,[
	"LMG_M200",
	"missiles_titan",
	"SmokeLauncher",
	"autocannon_35mm",
	"mortar_155mm_AMOS",
	"GMG_40mm",
	"HMG_127_APC",
	"Laserdesignator_mounted",
	"GBU12BombLauncher",
	"TruckHorn",
	"HMG_127",
	"SportCarHorn",
	"HMG_M2",
	"MiniCarHorn",
	"CarHorn",
	"missiles_titan_static",
	"GMG_UGV_40mm",
	"missiles_SCALPEL",
	"missiles_DAGR",
	"CMFlareLauncher",
	"HMG_01",
	"GMG_20mm",
	"mortar_82mm",
	"Rangefinder",
	"Laserdesignator",
	"arifle_SDAR_F",
	"arifle_TRG21_F",
	"Throw",
	"Put",
	"arifle_TRG20_F",
	"arifle_TRG20_ACO_F",
	"hgun_ACPC2_F",
	"Binocular",
	"arifle_Mk20_GL_ACO_F",
	"LMG_Mk200_F",
	"arifle_Mk20_F",
	"arifle_Mk20C_ACO_F",
	"arifle_TRG21_GL_F",
	"arifle_Mk20_MRCO_F",
	"launch_RPG32_F",
	"arifle_TRG21_MRCO_F"
],true] call BIS_fnc_addVirtualWeaponCargo;	
	
	
[_myBox,[
	"30Rnd_556x45_Stanag",
	"Chemlight_blue",
	"30Rnd_556x45_Stanag",
	"30Rnd_556x45_Stanag_Tracer_Yellow",
	"9Rnd_45ACP_Mag",
	"SmokeShellRed",
	"SmokeShellBlue",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_Smoke_Grenade_shell",
	"1Rnd_SmokeGreen_Grenade_shell",
	"1Rnd_SmokeRed_Grenade_shell",
	"1Rnd_SmokeBlue_Grenade_shell",
	"200Rnd_65x39_cased_Box",
	"APERSMine_Range_Mag",
	"HandGrenade",
	"MiniGrenade",
	"30Rnd_556x45_Stanag",
	"RPG32_F",
	"30Rnd_556x45_Stanag",
	"16Rnd_9x21_Mag",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_SmokeOrange_Grenade_shell",
	"SmokeShellOrange",
	"30Rnd_556x45_Stanag",
	"20Rnd_556x45_UW_mag",
	"SmokeShellRed",
	"ClaymoreDirectionalMine_Remote_Mag",
	"APERSTripMine_Wire_Mag",
	"Laserbatteries",
	"6Rnd_LG_scalpel",
	"2Rnd_GBU12_LGB",
	"100Rnd_127x99_mag_Tracer_Yellow"
],true] call BIS_fnc_addVirtualMagazineCargo;



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add BLUFOR gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"B_HMG_01_weapon_F",
	"B_GMG_01_weapon_F",
	"B_HMG_01_A_weapon_F",
	"B_GMG_01_A_weapon_F",
	"B_HMG_01_high_weapon_F",
	"B_GMG_01_high_weapon_F",
	"B_HMG_01_A_high_weapon_F",
	"B_GMG_01_A_high_weapon_F",
	"B_AT_01_weapon_F",
	"B_AA_01_weapon_F",
	"B_Mortar_01_weapon_F",
	"B_AssaultPack_rgr",
	"B_BergenC_grn",
	"B_Carryall_khk",
	"B_FieldPack_khk",
	"B_Kitbag_rgr",
	"B_OutdoorPack_blu",
	"B_TacticalPack_rgr",
	"B_Parachute"
],true] call BIS_fnc_addVirtualBackpackCargo;
	
[_myBox,[
	"U_B_CombatUniform_mcam_tshirt",
	"V_PlateCarrier1_rgr",
	"H_HelmetB",
	"NVGoggles",
	"U_B_CombatUniform_mcam_vest",
	"U_B_HeliPilotCoveralls",
	"U_B_PilotCoveralls",
	"U_B_CombatUniform_mcam",
	"U_Rangemaster",
	"H_Cap_headphones",
	"V_Rangemaster_belt",
	"V_BandollierB_rgr",
	"H_MilCap_mcamo",
	"V_PlateCarrierGL_rgr",
	"H_HelmetSpecB_blk",
	"V_PlateCarrier2_rgr",
	"H_HelmetB_grass",
	"H_HelmetB_desert",
	"H_HelmetSpecB",
	"H_HelmetB_sand",
	"V_PlateCarrierSpec_rgr",
	"H_HelmetB_light_desert",
	"H_HelmetB_light_sand",
	"H_PilotHelmetHeli_B",
	"H_HelmetSpecB_paint1",
	"H_HelmetSpecB_paint2",
	"V_Chestrig_rgr",
	"H_HelmetCrew_B",
	"U_Competitor",
	"H_PilotHelmetFighter_B",
	"H_CrewHelmetHeli_B",
	"B_UavTerminal",
	"U_B_Wetsuit",
	"V_RebreatherB",
	"G_B_Diving",
	"H_HelmetB_plain_mcamo",
	"H_Booniehat_mcamo",
	"H_HelmetB_light",
	"G_Shades_Black",
	"U_B_GhillieSuit",
	"U_B_CTRG_2",
	"V_PlateCarrierL_CTRG",
	"V_PlateCarrier_Kerry",
	"H_Helmet_Kerry",
	"U_B_survival_uniform",
	"U_B_CTRG_1",
	"V_PlateCarrierH_CTRG",
	"H_HelmetB_light_snakeskin",
	"H_Cap_khaki_specops_UK",
	"U_B_CTRG_3",
	"U_BG_Guerilla1_1",
	"U_BG_leader",
	"U_BG_Guerilla2_1",
	"U_BG_Guerilla2_3",
	"U_BG_Guerilla2_2",
	"U_BG_Guerilla3_1",
	"U_BG_Guerrilla_6_1",
	"U_I_G_Story_Protagonist_F",
	"H_Bandanna_khk_hs",
	"U_I_G_resistanceLeader_F",
	"V_I_G_resistanceLeader_F",
	"U_B_Protagonist_VR",
	"U_BasicBody"
],true] call BIS_fnc_addVirtualItemCargo;
	
[_myBox,[
	"arifle_MX_ACO_pointer_F",
	"hgun_P07_F",
	"arifle_MX_ACO_F",
	"arifle_MX_GL_ACO_F",
	"arifle_MX_SW_pointer_F",
	"arifle_MX_Hamr_pointer_F",
	"arifle_MX_GL_Hamr_pointer_F",
	"arifle_MXM_Hamr_pointer_F",
	"launch_NLAW_F",
	"arifle_MX_pointer_F",
	"arifle_MX_Holo_pointer_F",
	"arifle_MXC_Holo_pointer_F",
	"SMG_01_Holo_F",
	"launch_B_Titan_short_F",
	"launch_B_Titan_F",
	"arifle_MXC_F",
	"arifle_MXC_Aco_F",
	"hgun_Pistol_heavy_01_MRD_F",
	"arifle_MXC_Holo_F",
	"hgun_P07_snds_F",
	"arifle_MX_ACO_pointer_snds_F",
	"arifle_MXC_ACO_pointer_snds_F",
	"arifle_MX_RCO_pointer_snds_F",
	"srifle_EBR_DMS_pointer_snds_F",
	"arifle_MX_GL_Holo_pointer_snds_F",
	"srifle_LRR_camo_SOS_F",
	"arifle_MX_GL_Black_Hamr_pointer_F",
	"arifle_MX_Black_Hamr_pointer_F",
	"srifle_EBR_Hamr_pointer_F",
	"arifle_MX_SW_Black_Hamr_pointer_F",
	"arifle_TRG21_GL_MRCO_F"
],true] call BIS_fnc_addVirtualWeaponCargo;	
	
[_myBox,[
	"30Rnd_65x39_caseless_mag",
	"Chemlight_green",
	"30Rnd_65x39_caseless_mag",
	"100Rnd_65x39_caseless_mag",
	"30Rnd_65x39_caseless_mag",
	"30Rnd_65x39_caseless_mag_Tracer",
	"B_IR_Grenade",
	"30Rnd_65x39_caseless_mag",
	"NLAW_F",
	"30Rnd_45ACP_Mag_SMG_01",
	"30Rnd_65x39_caseless_mag",
	"11Rnd_45ACP_Mag",
	"Chemlight_green",
	"20Rnd_762x51_Mag",
	"7Rnd_408_Mag"
],true] call BIS_fnc_addVirtualMagazineCargo;

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Remove globally restricted gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-	

[_myBox,[
	"hgun_PDW2000_F",
	"arifle_Mk20C_F",
	"arifle_Mk20_GL_F",
	"arifle_Mk20_plain_F",
	"arifle_Mk20C_plain_F",
	"arifle_Mk20_GL_plain_F",
	"srifle_EBR_F",
	"srifle_GM6_F",
	"launch_I_Titan_F",
	"launch_I_Titan_short_F",
	"launch_Titan_F",
	"launch_Titan_short_F"
],true] call BIS_fnc_removeVirtualWeaponCargo;	
	
	
[_myBox,[
	"B_HMG_01_support_F",
	"B_HMG_01_support_high_F",
	"B_Mortar_01_support_F",
	"I_HMG_01_weapon_F",
	"I_GMG_01_weapon_F",
	"I_HMG_01_high_weapon_F",
	"I_GMG_01_high_weapon_F",
	"I_HMG_01_support_F",
	"I_HMG_01_support_high_F",
	"I_Mortar_01_weapon_F",
	"I_Mortar_01_support_F",
	"O_HMG_01_support_F",
	"O_GMG_01_support_high_F",
	"O_Mortar_01_support_F",
	"B_UAV_01_backpack_F",
	"O_UAV_01_backpack_F",
	"I_UAV_01_backpack_F"
],true] call BIS_fnc_removeVirtualBackpackCargo;


[_myBox,[
	"NLAW_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA",
	"RPG32_F",
	"RPG32_HE_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA"
],true] call BIS_fnc_removeVirtualMagazineCargo;
	

[_myBox,[
	"optic_NVS",
	"optic_Nightstalker",
	"optic_tws",
	"optic_tws_mg",
	"U_I_CombatUniform",
	"U_I_CombatUniform_shortsleeve",
	"U_I_CombatUniform_tshirt",
	"U_I_OfficerUniform",
	"U_I_GhillieSuit",
	"U_I_HeliPilotCoveralls",
	"U_I_pilotCoveralls",
	"U_I_Wetsuit",
	"U_IG_Guerilla1_1",
	"U_IG_Guerilla2_1",
	"U_IG_Guerilla2_2",
	"U_IG_Guerilla2_3",
	"U_IG_Guerilla3_1",
	"U_IG_Guerilla3_2",
	"U_IG_leader",
	"U_BG_Guerilla3_2",
	"U_OG_Guerilla3_2",
	"U_C_Poloshirt_blue",
	"U_C_Poloshirt_burgundy",
	"U_C_Poloshirt_stripped",
	"U_C_Poloshirt_tricolour",
	"U_C_Poloshirt_salmon",
	"U_C_Poloshirt_redwhite",
	"U_C_Commoner1_1",
	"U_C_Commoner1_2",
	"U_C_Commoner1_3",
	"U_C_Poor_1",
	"U_C_Poor_2",
	"U_C_Scavenger_1",
	"U_C_Scavenger_2",
	"U_C_Farmer",
	"U_C_Fisherman",
	"U_C_WorkerOveralls",
	"U_C_FishermanOveralls",
	"U_C_WorkerCoveralls",
	"U_C_HunterBody_grn",
	"U_C_HunterBody_brn",
	"U_C_Commoner2_1",
	"U_C_Commoner2_2",
	"U_C_Commoner2_3",
	"U_C_PriestBody",
	"U_C_Poor_shorts_1",
	"U_C_Poor_shorts_2",
	"U_C_Commoner_shorts",
	"U_C_ShirtSurfer_shorts",
	"U_C_TeeSurfer_shorts_1",
	"U_C_TeeSurfer_shorts_2",
	"U_NikosBody",
	"U_MillerBody",
	"U_KerryBody",
	"U_OrestesBody",
	"U_AttisBody",
	"U_AntigonaBody",
	"U_IG_Menelaos",
	"U_C_Novak",
	"U_OI_Scientist",
	"V_PlateCarrierIA1_dgtl",
	"V_PlateCarrierIA2_dgtl",
	"V_PlateCarrierIAGL_dgtl",
	"V_RebreatherIA",
	"V_TacVest_oli",
	"V_TacVest_camo",
	"V_TacVest_blk_POLICE",
	"V_TacVestIR_blk",
	"V_TacVestCamo_khk",
	"V_BandollierB_cbr",
	"V_BandollierB_oli",
	"H_HelmetIA",
	"H_HelmetIA_net",
	"H_HelmetIA_camo",
	"H_HelmetCrew_I",
	"H_CrewHelmetHeli_I",
	"H_PilotHelmetHeli_I",
	"H_PilotHelmetFighter_I",
	"H_Booniehat_dgtl",
	"H_Booniehat_indp",
	"H_MilCap_dgtl",
	"H_Cap_grn",
	"H_Cap_red",
	"H_Cap_blu",
	"H_Cap_tan",
	"H_Cap_blk",
	"H_Cap_grn_BI",
	"H_Cap_blk_Raven",
	"H_Cap_blk_ION",
	"H_Cap_blk_CMMG",
	"H_MilCap_rucamo",
	"H_MilCap_gry",
	"H_MilCap_blue",
	"H_Booniehat_grn",
	"H_Booniehat_tan",
	"H_Booniehat_dirty",
	"H_StrawHat",
	"H_StrawHat_dark",
	"H_Hat_blue",
	"H_Hat_brown",
	"H_Hat_camo",
	"H_Hat_grey",
	"H_Hat_checker",
	"H_Hat_tan",
	"H_Bandanna_surfer",
	"H_Bandanna_cbr",
	"H_Bandanna_sgg",
	"H_Bandanna_gry",
	"H_Bandanna_camo",
	"H_TurbanO_blk",
	"H_Shemag_khk",
	"H_Shemag_tan",
	"H_ShemagOpen_khk",
	"H_ShemagOpen_tan",
	"H_Beret_blk",
	"H_Beret_blk_POLICE",
	"H_Beret_red",
	"H_Beret_grn",
	"H_Watchcap_khk",
	"H_Watchcap_sgg",
	"H_BandMask_blk",
	"H_BandMask_khk",
	"H_BandMask_reaper",
	"H_BandMask_demon",
	"H_Shemag_olive_hs",
	"H_Cap_oli_hs",
	"I_UavTerminal",
	"H_Booniehat_khk_hs"
],true] call BIS_fnc_removeVirtualItemCargo;

diag_log format ["%1: Transport & Ambush: WEST Virtual Arsenal initiated for: %2:", time, _myBox];
